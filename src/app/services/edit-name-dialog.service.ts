import { Injectable } from '@angular/core';
import { SavedInputService, SavedInput } from './saved-input.service';
import { NotificationService } from './notification-service';

@Injectable({
  providedIn: 'root'
})
export class EditNameDialogService {

  public inputValue:string = '';
  public isOpen:boolean = false;
  private valueToSave:string;

  constructor(
    private saveInputSrv:SavedInputService,
    private notifSrv:NotificationService
  ) { }

  open(value:string){
    this.valueToSave = value;
    this.isOpen = true;
  }

  close(save:boolean){
    if (save){
      let input:SavedInput = {
        name : this.inputValue,
        input : this.valueToSave
      };
      this.saveInputSrv.saveInput(input);
      this.notifSrv.open('Mardown guardado');
    }
    this.isOpen = false;
  }


}
