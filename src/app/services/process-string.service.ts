import { Injectable } from '@angular/core';
import { isNumber } from 'util';
import { LineModel, TABLELINETYPE } from '../models/line.model';

const INLINEMARKREGEXP = /[*]+/;
const BOLDREGEX = /([*][*])(.*?)([*][*])+/;
const ITALICREGEX = /[*](.*?)[*]+/;
const CODEREGEX = /[`](.{1,})[`]+/;
const CHECKREGEX = /\[[\sx]\]+/;


@Injectable({
  providedIn: 'root'
})
export class ProcessStringService {

  private _output:string;
  private _currentLine;

  private _lines:string[];

  private _processed:string[];


  constructor() {

  }

  mdToHtml(input:string){
    this._processed = []; 
    if(!input) return;
    this._lines = input.split('\n');

    for (this._currentLine = 0; this._currentLine < this._lines.length; this._currentLine++) {
      let i = this._currentLine
      const element = this._lines[this._currentLine];
      this._processed.push(this._processLine(element));    

    }
    this._output = this._processed.join('\n');
    return this._output;
  }

  _processLine(line:string){
    let l = new LineModel(line);

    if (l.isHeader()) return this._parseHeader(l);

    if (l.isBlockQuote()) return this._parseBlockQuote(l);

    if (l.isListElement()) return this._processList(l);

    if (l.isTableElement()) return this._processTable(l);

    //if(l.isOrderedListElement()) return this._processOrderedList(l.content);
    
    
    l.content = this._parsedHorizontalLine(l.content);
    
    l.content = this._parseParagraph(l);
    

    return l.content;

  }

  _processBlock(line:LineModel){
    let response:string;

    if (line.isListElement()) return this._processList(line);
    
    return response;
  }

  _processList(line:LineModel):string {

    let currentLine = new LineModel(this._lines[this._currentLine]);
    this._addElementToOutput(`<${line.blockTag}>`);

    let actualType = line.type;

    while (currentLine.isListElement() && currentLine.type == actualType) {
      let content = line.isOrderedListElement() ? currentLine.content.substring(3) : currentLine.content.substring(2);
      if (CHECKREGEX.test(content)){
        let lastLine = this._processed[this._processed.length-1];
        if (lastLine == "<ul>") this._processed[this._processed.length-1] = '<ul style="list-style:none;">';
        let checked:boolean = content[1] ==='x';
        content = content.substr(3);
        let id = "check_" + this._processed.length
        content = `
          <div class="custom-control custom-checkbox">
            <input id="${id}"  ${checked ? "checked" : ""} type="checkbox" class="custom-control-input">
            <label for="${id}" class="custom-control-label" >${content}</label>
          </div>
        `
      }
      this._addElementToOutput(`<${currentLine.tag} class="${currentLine.classes}">${content}</${currentLine.tag}>`, true)

       if (this._lines.length > 0){
         currentLine = new LineModel(this._lines[this._currentLine]);
       }else{
         break;
       }
    }
    this._currentLine--;
    return `</${line.blockTag}>`;
  }

  _processTable(line:LineModel):string{

    this._addElementToOutput('<table class="table mb-1 table-striped">');
    this._addElementToOutput('<thead>');
    // this._addElementToOutput('<tr>');

    let currentLine = line;
    let lineType = TABLELINETYPE.header;
    while (currentLine.isTableElement()) {
      if (currentLine.isTableSeparator()){
        this._currentLine++;
        currentLine = new LineModel(this._lines[this._currentLine]);
        // this._addElementToOutput('</tr>');
        this._addElementToOutput('</thead>');
        this._addElementToOutput('<tbody>');
        lineType = TABLELINETYPE.body;
        continue;
      }

      this._addElementToOutput(this._parseTableLine(currentLine.content,lineType));
      
      if (this._lines.length > 0){
        this._currentLine++
        currentLine = new LineModel(this._lines[this._currentLine]);
      }else{
        break;
      }
    }
    
    
    this._currentLine--;
    this._processed.push('</tbody>');
    return '</table>';
  }

  _parseTableLine(line:string, type:TABLELINETYPE){
    let tag:string;
    if (type == TABLELINETYPE.body) tag = 'td';
    if (type == TABLELINETYPE.header) tag = 'th';

    let a = line.split('|');
    let aResponse:string[] = ['<tr>'];

    for (let i = 1; i < a.length-1; i++) {
      const element = a[i];
      aResponse.push(`<${tag}>${element}</${tag}>`); 
    }

    aResponse.push('</tr>')

    return aResponse.join('\n');
  }

  _parsedHorizontalLine(line:string){
    if (line.length >=3 ){
      let r = line.replace(/[-]/gi,'');
      if (r.length > 0){
        return line;
      }
      return '<hr>';
    }
    return line;
  }

  _parseParagraph(line:LineModel, cssClass:string = ""):string{
    let response:string = `<${line.tag} class="${cssClass}">${line.content}</${line.tag}>`;
    return response;
  }
  _parseHeader(line:LineModel):string{

    let content:string = line.content.replace(/[#]/gi,'');

    return `<${line.tag}>${content}</${line.tag}>`;
  }

  _parseCode(line: string): string {
    let r:string = line.replace(CODEREGEX, (substring) => {
      return substring.replace('`', '<code>').replace('`', '</code>')
    });

    if (CODEREGEX.test(r)){
      r = this._parseCode(r);
    }

    return r;
  }

  _parseItalic(line:string):string{
    let r:string  = line.replace(ITALICREGEX,(substring)=> {
      return substring.replace('*', '<span class="font-italic">').replace('*', '</span>');
    });

    if(ITALICREGEX.test(r)){
      r = this._parseItalic(r);
    }   
    return r
  }

  _parseBlockQuote(line:LineModel):string{
    // > sanhljfhslakdfjhasf
    line.content = line.content.replace('>','');
    let r = `
      <${line.tag} class="${line.classes}">
        <p class="mb-0">${line.content}</p>
      </${line.tag}>
    `;
    return r;
  }

  _addElementToOutput(element:string, nextLine = false){
    this._processed.push(element);
    if (nextLine) this._currentLine++;
  }
}
