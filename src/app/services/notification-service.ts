import { Injectable } from "@angular/core";

@Injectable({
    providedIn:'root'
})
export class NotificationService {
    private _toast = document.getElementById('toast');
    private _showClass = 'fadeInRight';
    private _hideClass = 'fadeOutUp';
    public msn:string;

    public show:boolean;
    constructor(){}

    open(msn:string, duration?:number){
        this.msn = msn;
        this.show = true;
        if (duration > 0){
            setTimeout(()=>this.close(),duration)
        }
    }

    close(){
        this.show = false;
    }
}