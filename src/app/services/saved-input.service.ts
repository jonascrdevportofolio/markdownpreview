import { Injectable } from "@angular/core";

export interface SavedInput {
    input?:string
    name?:string
}

@Injectable({
    providedIn: 'root'
})
export class SavedInputService{

    selectedInput:string;

    inputs:SavedInput[];

    private _storageItem:string;

    constructor(){
        this.loadSavedInput();
    }

    loadSavedInput(){
        this.inputs = []
        let storage:any[] = JSON.parse(localStorage.getItem(this._storageItem)) || [];

        for (let i = 0; i < storage.length; i++) {
            const element = storage[i];
            let input:SavedInput = {
                input: element["input"],
                name: element["name"]
            };
            this.inputs.push(input);
        }
    }

    saveInput(input:SavedInput){
        this.inputs.push(input);
        this._saveToStorage();
    }

    deleteInput(i:number){
        this.inputs.splice(i,1);
        this._saveToStorage();
    }

    editName(name:string, i:number){
        this.inputs[i].name = name;
        this._saveToStorage();
    }

    private _saveToStorage(){
        localStorage.setItem(this._storageItem, JSON.stringify(this.inputs));
    }

}