const INLINEMARKREGEXP = /[*]+/;
const BOLDREGEX = /([*][*])(.*?)([*][*])+/;
const ITALICREGEX = /[*](.*?)[*]+/;
const CODEREGEX = /[`](.{1,})[`]+/;
const LINKREGEXP = /\[[^\sx].*\]\(.*?\)+/;
const IMAGEREGEXP = /\!\[.*\]\(.*?\)+/;
const TABLEHEADERSEP =  /^\|(-{3,})\|/

export enum BLOCKTYPE {
    orderedList = 0,
    unorderedList
}

export enum TABLELINETYPE {
    header,
    body
}

export class LineModel {
    tag:string;
    blockTag:string;
    classes:string = '';
    type:BLOCKTYPE;
    constructor(public content:string){
        this._start();
    }

    private _start(){
        this._parseInlineElement();
        if (this.isBlockQuote()){
            this.tag = 'blockquote'
            this.classes = 'blockquote border-left border-info p-2 bg-light';
            return;
        }

        if (this.isHeader()){
            let lastHash:number = this.content.lastIndexOf('#');
            if (lastHash > 5) lastHash = 5;
            let headerLevel:number = lastHash+1; //this.content.split(' ',2)[0].length;
            this.tag = `h${headerLevel}`;
            return;
        }

        if (this.isOrderedListElement()){
            this.tag = 'li';
            this.blockTag = 'ol';
            this.type = BLOCKTYPE.orderedList;
            return;
        }

        if (this.isUnorderedListElement()){
            this.tag = 'li';
            this.blockTag = 'ul';
            this.type = BLOCKTYPE.unorderedList;

            return;
        }


        this.tag = 'p';   
    }

    _parseInlineElement(){
        let r:string;
        r = this._parseBold(this.content);
        r = this._parseItalic(r);
        r = this._parseCode(r);
        r = this._parseImage(r);
        r = this._parseLink(r);
        
        this.content = r;
    }

    isBlockQuote(){
        return this.content.startsWith('>');
    }
    
    isHeader():boolean{
        return this.content.startsWith('#');
    }

    isOrderedListElement():boolean{
        let first = this.content.substring(0,1);
        let second = this.content.substring(1,2);

        if ( !isNaN( Number(first) ) && second === '.') return true;
                
        return false
    }

    isUnorderedListElement():boolean{        
        let first = this.content.substring(0,1);
        let second = this.content.substring(1,2);


        if (first === '-' && second === ' ') return true;
        
        return false
    }

    isBlockElement(){
        if (this.isOrderedListElement) return true;
        if (this.isUnorderedListElement) return true;
        if (this.isTableElement) return true;

        return false;

    }

    isListElement() {
        return this.isOrderedListElement() || this.isUnorderedListElement();
    }

    isTableElement(){
        return this.content.startsWith('|');
    }

    isTableSeparator(){
        return TABLEHEADERSEP.test(this.content);
    }

    _parseBold(line:string):string{
        let r:string = '';
        try {
            r = line.replace(BOLDREGEX,(substring)=> {
                let response = substring.replace('**','<span class="font-weight-bold">').replace('**', '</span>');
                
                return response;
            });
            if (BOLDREGEX.test(r)){
                r = this._parseBold(r);
            }
            
        } catch (error) {
            
        }

        return r;
    }

    _parseItalic(line:string):string{
        let r:string  = line.replace(ITALICREGEX,(substring)=> {
            return substring.replace('*', '<span class="font-italic">').replace('*', '</span>');
        });

        if(ITALICREGEX.test(r)){
            r = this._parseItalic(r);
        }   
        return r
    }
    _parseCode(line: string): string {
        let r:string = line.replace(CODEREGEX, (substring) => {
            return substring.replace('`', '<code>').replace('`', '</code>')
        });

        if (CODEREGEX.test(r)){
            r = this._parseCode(r);
        }

        return r;
    }

    _parseLink(line:string):string{
        let r:string = line.replace(LINKREGEXP, (substring) => {
             let startLinkIndex: number = substring.indexOf('(');
             let endTextIndex = substring.lastIndexOf(']');

             let link = substring.substring(startLinkIndex+1,substring.length-1);

             let text = substring.substring(1,endTextIndex);

             console.log(text);
             

             if (LINKREGEXP.test(text)){
                 text = this._parseLink(text);
             }

            return `
            <a href="${link}">
                ${text}
            </a>
         `
        })

        return r;
    }

    _parseImage(line:string):string{
        let r:string = line.replace(IMAGEREGEXP, (substring) => {
            substring = substring.substring(0,substring.length-1);
            console.log(substring);
            
            let startSrcIndex: number = substring.indexOf('(');
            let endAltIndex = substring.lastIndexOf(']');
            let src = substring.substring(startSrcIndex+1,substring.length);
            let alt = substring.substring(2,endAltIndex);

            return `
                <img src="${src}" class="img-fluid" alt="${alt}" />
            `
        })

        return r;
    }



}