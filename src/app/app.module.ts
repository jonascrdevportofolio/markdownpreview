import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import { ClipboardModule } from 'ngx-clipboard';


import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { InputComponent } from './components/input/input.component';
import { OutputComponent } from './components/output/output.component';
import { SavedInputComponent } from './components/saved-input/saved-input.component';
import { EditInputNameDialogComponent } from './components/edit-input-name-dialog/edit-input-name-dialog.component';
import { NotificationComponent } from './components/notification/notification.component';
import { SanitizehtmlPipe } from './pipes/sanitizehtml.pipe';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    InputComponent,
    OutputComponent,
    SavedInputComponent,
    EditInputNameDialogComponent,
    NotificationComponent,
    SanitizehtmlPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ClipboardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
