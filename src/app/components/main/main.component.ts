import { Component, OnInit, ViewChild } from '@angular/core';
import { ProcessStringService } from 'src/app/services/process-string.service';
import { isString } from 'util';
import { SavedInputService } from 'src/app/services/saved-input.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  public _output:string;
  public _colClass:string = 'col-md-6';

  constructor(
    private _processSrv:ProcessStringService,
    public _savedInputSrv:SavedInputService
  ) {
   }

  ngOnInit() {
  }

  onInputEmit(value:any){
    if (isString(value)){
      this._output = this._processSrv.mdToHtml(value);
    }
  }

  onSelectLayout(col:number){
    this._colClass = `col-md-${col}`
  }

  onClickInfo(){
    console.info('This feature will be available in the next version');
    
  }

}
