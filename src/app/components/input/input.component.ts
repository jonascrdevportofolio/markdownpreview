import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { NotificationService } from 'src/app/services/notification-service';
import { SavedInputService, SavedInput } from 'src/app/services/saved-input.service';
import { EditNameDialogService } from 'src/app/services/edit-name-dialog.service';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

  @Output() input:EventEmitter<string> = new EventEmitter();
  @Input() inputValue:string = '';
  
  constructor(
    protected _notificationSrv:NotificationService,
    protected _savedInput:SavedInputService,
    protected _editDialog:EditNameDialogService
  ) { }
  
  ngOnInit() {}

  onInputChange(){
    this.input.emit(this.inputValue);
  }

  showNotification(){
    this._notificationSrv.open('You can now paste your markdown input where ever you need')
  }

  saveInput(){
    this._editDialog.open(this.inputValue);
    /*
    let input:SavedInput = {
      name : 'Input guardado',
      input : this.inputValue
    }
    this._savedInput.saveInput(input);
    this._notificationSrv.show('Mardown guardado')
    */
  }
}
