import { Component, OnInit } from '@angular/core';
import { EditNameDialogService } from 'src/app/services/edit-name-dialog.service';

@Component({
  selector: 'app-edit-input-name-dialog',
  templateUrl: './edit-input-name-dialog.component.html',
  styleUrls: ['./edit-input-name-dialog.component.css']
})
export class EditInputNameDialogComponent implements OnInit {

  modalClass:string = 'hide';

  constructor(
    protected editName:EditNameDialogService
  ) { }

  ngOnInit() {
  }

  onSave(){

  }



}
