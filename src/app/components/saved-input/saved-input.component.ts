import { Component, OnInit } from '@angular/core';
import { SavedInputService } from 'src/app/services/saved-input.service';

@Component({
  selector: 'app-saved-input',
  templateUrl: './saved-input.component.html',
  styleUrls: ['./saved-input.component.css']
})
export class SavedInputComponent implements OnInit {

  constructor(
    protected savedInputSrv:SavedInputService
  ) { }

  ngOnInit() {
  }

  delete(i:number){
    this.savedInputSrv.deleteInput(i);
  }

  editInput(i:number){
    //this.savedInputSrv.editName(name,i);
  }

  onRowClick(content:string){
    this.savedInputSrv.selectedInput = content;
  }

}
