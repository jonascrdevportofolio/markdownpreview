import { Component, OnInit, Input } from '@angular/core';
import { ClipboardService } from 'ngx-clipboard';
import { NotificationService } from 'src/app/services/notification-service';

@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.css']
})
export class OutputComponent implements OnInit {

  @Input() processedHtml:string = '<p></p>';

  constructor(private _clipboard:ClipboardService, private _notificationSrv:NotificationService) { }

  ngOnInit() {
  }

  copyHTML(){
    this._clipboard.copyFromContent(this.processedHtml);
    this._notificationSrv.open('You can now paste your HTML where ever you need', 5000);
  }

}